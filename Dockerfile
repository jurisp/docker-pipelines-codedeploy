FROM alpine:3.7

RUN apk add --no-cache \
      zip \
      bash \
      py-pip \
      python \
      gettext \
    && pip install awscli \
    && apk --purge del -v py-pip

COPY scripts/aws_code_deploy.sh /usr/local/bin
